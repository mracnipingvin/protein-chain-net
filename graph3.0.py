from tkinter import *
import itertools
import math
import pickle


class Graph(object):
    def __init__(self, graph_sizex, graph_sizey, chain_length, *args, **kwargs):
        self.graph_sizex = graph_sizex
        self.graph_sizey = graph_sizey
        self.chain_length = chain_length
        self.graph_dict = self.make_square_graph(self.graph_sizex, self.graph_sizey)

        self.node_list = []
        self.__generate_edges()

        self.shape_list = []

        self.matrix = self.generate_matrix(self.chain_length, self.chain_length, self.chain_length)
        self.alphabet = self.generate_alphabet(self.chain_length, self.chain_length, self.chain_length)

        self.matrix_list = []
        self.stable = True
        self.graph_draw = GraphDraw(self, 750, self.graph_sizex)

    def make_square_graph(self, sizex, sizey):
        dicti = {i: [] for i in range(sizex * sizey)}
        for i in dicti:
            if not ((int(i) % sizex) - 1 < 0):
                dicti[i].append(int(i) - 1)
            if not (int(i) - sizex < 0):
                dicti[i].append(int(i) - sizex)
            if not (int(i) + 1 >= sizex * (i // sizex + 1)):
                dicti[i].append(int(i) + 1)
            if not (int(i) + sizex >= sizey * sizex):
                dicti[i].append(int(i) + sizex)
        return dicti

    def generate_matrix(self, length_f, length_s, length_t):
        matrix = [[[0 for i in range(length_s * 2)] for j in range(length_f)],
                  [[0 for i in range(length_t * 2)] for j in range(length_f)],
                  [[0 for i in range(length_t * 2)] for j in range(length_s)]]
        return matrix

    def generate_alphabet(self, length_f, length_s, length_t):
        alphabet = 'abcdefghijklmnopqrstuvwxyz'
        f_alphabet = alphabet[:length_f]
        s_alphabet = alphabet[length_f:length_s + length_f] + alphabet[length_f:length_s + length_f].upper()
        t_alphabet = (alphabet[length_f + length_s:length_f + length_s + length_t] +
                      alphabet[length_f + length_s:length_f + length_s + length_t].upper())
        return [f_alphabet, s_alphabet, t_alphabet]

    def shape_generator(self, shape, movex, movey, orientation, startx=0, starty=0, first=1):

        xcount = 0
        ycount = 0
        foo = Shape(self,
                    shape,
                    self.graph_sizex * (ycount * movey) + starty * self.graph_sizex + (xcount * movex) + startx,
                    orientation,
                    first)
        size = foo.get_size()
        while ycount * movey + starty < self.graph_sizey:
            if not self.check_if_stable():
                return
            foo = Shape(self,
                        shape,
                        self.graph_sizex * (ycount * movey) + starty * self.graph_sizex + (xcount * movex) + startx,
                        orientation,
                        first)
            xcount = 0

            if foo.start_point + (size[2] * self.graph_sizex) > self.graph_sizey * self.graph_sizex:
                break
            elif foo.start_point + (size[3] * self.graph_sizex) < 0:
                pass
            else:
                while xcount * movex + startx < self.graph_sizex:
                    foo = Shape(self, shape, self.graph_sizex * (ycount * movey) + starty * self.graph_sizex + (
                        xcount * movex) + startx, orientation, first)
                    if (foo.start_point + size[0]) // self.graph_sizex > foo.start_point // self.graph_sizex:
                        break
                    elif (foo.start_point + size[1]) // self.graph_sizex < foo.start_point // self.graph_sizex:
                        pass
                        xcount += 1
                    else:
                        xcount += 1
                        yield (foo)
            ycount += 1
            if not self.check_if_stable():
                return

    def draw_layer(self, shape, movex, movey, orientation, startx=0, starty=0, first=1):
        for foo in self.shape_generator(shape, movex, movey, orientation, startx, starty, first):
            self.shape_list.append(foo)
            self.graph_draw.shape_queue.append(foo)
            self.draw_shape(foo)

    def draw_layers(self, shape1, movex1, movey1, orientation1,
                    shape2, movex2, movey2, orientation2, startx2, starty2,
                    shape3=None, movex3=None, movey3=None, orientation3=None, startx3=None, starty3=None):
        first_generator = self.shape_generator(shape1, movex1, movey1, orientation1)
        second_generator = self.shape_generator(shape2, movex2, movey2, orientation2, startx2, starty2, 2)
        if shape3:
            third_generator = self.shape_generator(shape3, movex3, movey3, orientation3, startx3, starty3, 3)

        generators = [1,1,1]
        while generators != [0,0,0]:
            try:
                foo = next(first_generator)
                self.shape_list.append(foo)
                self.draw_shape(foo)
                if not self.stable:
                    self.stable = True
                    break
            except StopIteration:
                generators[0] = 0

            try:
                foo = next(second_generator)
                self.shape_list.append(foo)
                self.draw_shape(foo)
                if not self.stable:
                    self.stable = True
                    break
            except StopIteration:
                generators[1] = 0

            if shape3:
                try:
                    foo = next(third_generator)
                    self.shape_list.append(foo)
                    self.draw_shape(foo)
                    if not self.stable:
                        self.stable = True
                        break
                except StopIteration:
                    generators[2] = 0
            else:
                generators[2] = 0

        if self.check(33, 66) and self.stable == True:
            self.matrix_list.append(
                [self.matrix, shape1, movex1, movey1, orientation1, shape2, movex2, movey2, orientation2, startx2,
                 starty2, shape3, movex3, movey3, orientation3, startx3, starty3])
            pickle.dump(g.matrix_list, open("matrix_list.p", "wb"))
            for i in g.matrix[0]:
                print(i)
            self.draw()

        self.raise_not_stable()
        self.stable = True

    def make_connection(self, point1, point2, letter=None, orientation=None):
        for i in self.edge_list:
            if (i[0] == point1 and i[1] == point2) or (i[0] == point2 and i[1] == point1):
                i[2] += 1
                i[3] = len(self.shape_list)
                if letter:
                    if i[4] == 0:
                        i[4] = letter
                        i[5] = orientation
                    elif i[4] == "checked":
                        self.raise_not_stable()
                        return
                    else:
                        second_letter = i[4]

                        if self.alphabet[0].find(letter) != -1:
                            alphabet = 0
                        elif self.alphabet[1].find(letter) != -1:
                            alphabet = 1
                        elif self.alphabet[2].find(letter) != -1:
                            alphabet = 2

                        if self.alphabet[0].find(i[4]) != -1:
                            second_alphabet = 0
                        elif self.alphabet[1].find(i[4]) != -1:
                            second_alphabet = 1
                        elif self.alphabet[2].find(i[4]) != -1:
                            second_alphabet = 2
                        if alphabet > second_alphabet:
                            letter, second_letter = second_letter, letter
                            alphabet, second_alphabet = second_alphabet, alphabet
                        elif alphabet == second_alphabet:
                            self.raise_not_stable()
                            print("not stable")
                            return
                        if 1 in self.matrix[alphabet + second_alphabet - 1][self.alphabet[alphabet].index(letter)]:
                            if i[5] != orientation:
                                if self.matrix[alphabet + second_alphabet - 1][self.alphabet[alphabet].index(letter)][
                                            self.alphabet[second_alphabet].index(second_letter) + len(
                                        self.alphabet[second_alphabet]) // 2] == 1:
                                    pass
                                else:
                                    self.raise_not_stable()
                                    print("not stable")
                                    return
                            else:
                                if (self.matrix[alphabet + second_alphabet - 1][self.alphabet[alphabet].index(letter)]
                                    [self.alphabet[second_alphabet].index(second_letter)] == 1):
                                    pass
                                else:
                                    self.raise_not_stable()
                                    print("not stable")
                                    return
                        else:
                            if i[5] != orientation:
                                self.matrix[alphabet + second_alphabet - 1][self.alphabet[alphabet].index(letter)][
                                    self.alphabet[second_alphabet].index(second_letter) + len(
                                        self.alphabet[second_alphabet]) // 2] = 1
                            else:
                                self.matrix[alphabet + second_alphabet - 1][self.alphabet[alphabet].index(letter)][
                                    self.alphabet[second_alphabet].index(second_letter)] = 1
                        i[4] = "checked"

    def raise_not_stable(self):
        
        self.edge_list = []
        self.shape_list = []
        self.node_list = []
        self.__generate_edges()
        self.matrix = self.generate_matrix(self.chain_length,self.chain_length,self.chain_length)
        self.graph_draw = GraphDraw(self, 750, self.graph_sizex)

        self.stable = False

    def check_if_stable(self):
        if not self.stable:
            return False
        else:
            return True

    def check(self, first_point, second_point):
        for i in self.edge_list:
            if (i[0] % self.graph_sizex >= first_point % self.graph_sizex and
                            i[0] % self.graph_sizex <= second_point % self.graph_sizex and
                            i[1] % self.graph_sizex >= first_point % self.graph_sizex and
                            i[1] % self.graph_sizex <= second_point % self.graph_sizex and
                        i[0] >= first_point and i[1] >= first_point and
                        i[0] <= second_point and i[1] <= second_point):
                if (i[2] == 2):
                    pass
                else:
                    return (False)
        return (True)

    def draw_shape(self, shape):
        _start_point = shape.start_point
        _orientation = shape.orientation
        count = 0

        if shape.first == 1:
            alphabet = 0
        elif shape.first == 2:
            alphabet = 1
        else:
            alphabet = 2

        # preveri st. koncev v ogliscu
        self.node_list[_start_point][1] += 1
        if self.node_list[_start_point][1] > 2:
            self.raise_not_stable()
            return
        if _orientation == 0:
            self.make_connection(_start_point, _start_point + 1, self.alphabet[alphabet][count], _orientation)
            _start_point += 1
        elif _orientation == 1:
            self.make_connection(_start_point, _start_point + self.graph_sizex, self.alphabet[alphabet][count],
                                 _orientation)
            _start_point = _start_point + self.graph_sizex
        elif _orientation == 2:
            self.make_connection(_start_point, _start_point - 1, self.alphabet[alphabet][count], _orientation)
            _start_point -= 1
        elif _orientation == 3:
            self.make_connection(_start_point, _start_point - self.graph_sizex, self.alphabet[alphabet][count],
                                 _orientation)
            _start_point = _start_point - self.graph_sizex
        count += 1

        if not self.check_if_stable():
            return

        for i in shape.shape:
            if i == -1:
                _orientation -= 1
                if _orientation < 0:
                    _orientation += 4
            elif i == 1:
                _orientation += 1
                if _orientation >= 4:
                    _orientation -= 4

            if _orientation == 0:
                self.make_connection(_start_point, _start_point + 1, self.alphabet[alphabet][count], _orientation)
                _start_point += 1
            elif _orientation == 1:
                self.make_connection(_start_point, _start_point + self.graph_sizex, self.alphabet[alphabet][count],
                                     _orientation)
                _start_point = _start_point + self.graph_sizex
            elif _orientation == 2:
                self.make_connection(_start_point, _start_point - 1, self.alphabet[alphabet][count], _orientation)
                _start_point -= 1
            elif _orientation == 3:
                self.make_connection(_start_point, _start_point - self.graph_sizex, self.alphabet[alphabet][count],
                                     _orientation)
                _start_point = _start_point - self.graph_sizex
            count += 1
            if not self.check_if_stable():
                return

        self.node_list[_start_point][1] += 1
        if self.node_list[_start_point][1] > 2:
            self.raise_not_stable()
            return
        self.shape_list.append(shape)
        self.graph_draw.shape_queue.append(shape)

    def __generate_edges(self):
        edges = []
        for vertex in self.graph_dict:
            self.node_list.append([vertex, 0])
            for neighbour in self.graph_dict[vertex]:
                if [neighbour, vertex, 0, 0, 0, 0] not in edges:
                    edges.append([vertex, neighbour, 0, 0, 0, 0])
        self.edge_list = edges
        return edges

    def draw(self):
        self.graph_draw.draw()


class Shape(object):
    def __init__(self, graph, shape, start_point, orientation, first=1):
        self.graph = graph
        self.shape = shape
        self.start_point = start_point
        self.orientation = orientation
        self.sizex = graph.graph_sizex

        self.first = first
        if first == 1:
            self.alphabet = graph.alphabet[0]
        elif first == 2:
            self.alphabet = graph.alphabet[1]
        else:
            self.alphabet = graph.alphabet[2]

        self.edge_list = self.init_edges()

    def init_edges(self):
        _start_point = self.start_point
        _orientation = self.orientation
        temp = []
        count = 0

        if _orientation == 0:
            temp.append([_start_point, _start_point + 1, _orientation, self.alphabet[count]])
            _start_point += 1
        if _orientation == 1:
            temp.append([_start_point, _start_point + self.sizex, _orientation, self.alphabet[count]])
            _start_point = _start_point + self.sizex
        if _orientation == 2:
            temp.append([_start_point, _start_point - 1, _orientation, self.alphabet[count]])
            _start_point -= 1
        if _orientation == 3:
            temp.append([_start_point, _start_point - self.sizex, _orientation, self.alphabet[count]])
            _start_point = _start_point - self.sizex
        for i in self.shape:
            count += 1
            if i == -1:
                _orientation -= 1
                if _orientation < 0:
                    _orientation += 4
            elif i == 1:
                _orientation += 1
                if _orientation >= 4:
                    _orientation -= 4
            if _orientation == 0:
                temp.append([_start_point, _start_point + 1, _orientation, self.alphabet[count]])
                _start_point += 1
            if _orientation == 1:
                temp.append([_start_point, _start_point + self.sizex, _orientation, self.alphabet[count]])
                _start_point = _start_point + self.sizex
            if _orientation == 2:
                temp.append([_start_point, _start_point - 1, _orientation, self.alphabet[count]])
                _start_point -= 1
            if _orientation == 3:
                temp.append([_start_point, _start_point - self.sizex, _orientation, self.alphabet[count]])
                _start_point = _start_point - self.sizex

        return temp

    def get_size(self):
        _orientation = self.orientation
        x = 0
        y = 0
        minx = x
        maxx = x
        miny = y
        maxy = y
        if _orientation == 0:
            x += 1
        elif _orientation == 1:
            y += 1
        elif _orientation == 2:
            x -= 1
        elif _orientation == 3:
            y -= 1
        if x < minx:
            minx = x
        elif y < miny:
            miny = y
        elif x > maxx:
            maxx = x
        elif y > maxy:
            maxy = y
        for i in self.shape:
            if i == 1:
                _orientation += 1
                if _orientation == 4:
                    _orientation = 0
            elif i == -1:
                _orientation -= 1
                if _orientation == -1:
                    _orientation = 3
            if _orientation == 0:
                x += 1
            elif _orientation == 1:
                y += 1
            elif _orientation == 2:
                x -= 1
            elif _orientation == 3:
                y -= 1
            if x < minx:
                minx = x
            elif y < miny:
                miny = y
            elif x > maxx:
                maxx = x
            elif y > maxy:
                maxy = y
        return [maxx, minx, maxy, miny]


class GraphDraw(object):
    def __init__(self, graph, size, sizex):

        self.graph = graph
        self.size = size
        self.sizex = sizex

        self.canvas_width = self.size
        self.canvas_height = self.size

        self.node_map = self.generate_node_map()

        self.line_thickness = 5
        self.circle_radius = 10

        self.shape_queue = []

    def draw(self):
        master = Tk()
        self.canvas = Canvas(master, width=self.canvas_width, height=self.canvas_height)
        self.canvas.pack()
        self.draw_node_map()

        for i in self.shape_queue:
            self.draw_shape(i)

        mainloop()

    def create_circle(self, x, y, r, **kwargs):
        return self.canvas.create_oval(x - r, y - r, x + r, y + r, **kwargs)

    def generate_node_map(self):
        distance = self.size // (self.sizex + 1)
        node_map = []
        for j in range(self.sizex):
            for i in range(self.sizex):
                node_map.append([distance + i * distance, distance + j * distance])
        return node_map

    def draw_node_map(self):
        for i in self.node_map:
            self.create_circle(i[0], i[1], self.circle_radius, fill="cyan")

    def connect_nodes(self, edge, first=1, direction=None):
        start_node = edge[0]
        end_node = edge[1]
        letter = edge[3]
        if self.node_map[end_node][0] == self.node_map[start_node][0]:
            orientation = 'y'
        else:
            orientation = 'x'
        offset = self.line_thickness
        text_offset = 15
        if first == 1:
            if direction == None:
                self.canvas.create_line(self.node_map[start_node][0] - offset,
                                        self.node_map[start_node][1] - offset,
                                        self.node_map[end_node][0] - offset,
                                        self.node_map[end_node][1] - offset,
                                        fill="#FF0000", width=5)

            elif direction == 0:
                self.canvas.create_line(self.node_map[start_node][0] + self.circle_radius,
                                        self.node_map[start_node][1] - offset,
                                        self.node_map[end_node][0] - offset,
                                        self.node_map[end_node][1] - offset,
                                        fill="#FF0000", width=5)
            elif direction == 1:
                self.canvas.create_line(self.node_map[start_node][0] - offset,
                                        self.node_map[start_node][1] + self.circle_radius,
                                        self.node_map[end_node][0] - offset,
                                        self.node_map[end_node][1] - offset,
                                        fill="#FF0000", width=5)
            elif direction == 2:
                self.canvas.create_line(self.node_map[start_node][0] - self.circle_radius,
                                        self.node_map[start_node][1] - offset,
                                        self.node_map[end_node][0] - offset,
                                        self.node_map[end_node][1] - offset,
                                        fill="#FF0000", width=5)
            elif direction == 3:
                self.canvas.create_line(self.node_map[start_node][0] - offset,
                                        self.node_map[start_node][1] - self.circle_radius,
                                        self.node_map[end_node][0] - offset,
                                        self.node_map[end_node][1] - offset,
                                        fill="#FF0000", width=5)
            if orientation == 'x':
                self.canvas.create_text((self.node_map[start_node][0] + self.node_map[end_node][0]) // 2,
                                        self.node_map[start_node][1] - text_offset,
                                        text=letter, fill="#FF0000")
            else:
                self.canvas.create_text(self.node_map[start_node][0] - text_offset,
                                        (self.node_map[start_node][1] + self.node_map[end_node][1]) // 2,
                                        text=letter, fill="#FF0000")
        elif first == 2:
            if direction == None:
                self.canvas.create_line(self.node_map[start_node][0] + offset,
                                        self.node_map[start_node][1] + offset,
                                        self.node_map[end_node][0] + offset,
                                        self.node_map[end_node][1] + offset,
                                        fill="#00FF00", width=5)
            elif direction == 0:
                self.canvas.create_line(self.node_map[start_node][0] + self.circle_radius,
                                        self.node_map[start_node][1] + offset,
                                        self.node_map[end_node][0] + offset,
                                        self.node_map[end_node][1] + offset,
                                        fill="#00FF00", width=5)
            elif direction == 1:
                self.canvas.create_line(self.node_map[start_node][0] + offset,
                                        self.node_map[start_node][1] + self.circle_radius,
                                        self.node_map[end_node][0] + offset,
                                        self.node_map[end_node][1] + offset,
                                        fill="#00FF00", width=5)
            elif direction == 2:
                self.canvas.create_line(self.node_map[start_node][0] - self.circle_radius,
                                        self.node_map[start_node][1] + offset,
                                        self.node_map[end_node][0] + offset,
                                        self.node_map[end_node][1] + offset,
                                        fill="#00FF00", width=5)
            elif direction == 3:
                self.canvas.create_line(self.node_map[start_node][0] + offset,
                                        self.node_map[start_node][1] - self.circle_radius,
                                        self.node_map[end_node][0] + offset,
                                        self.node_map[end_node][1] + offset,
                                        fill="#00FF00", width=5)
            if orientation == 'x':
                self.canvas.create_text((self.node_map[start_node][0] + self.node_map[end_node][0]) // 2,
                                        self.node_map[start_node][1] + text_offset,
                                        text=letter, fill="#00FF00")
            else:
                self.canvas.create_text(self.node_map[start_node][0] + text_offset,
                                        (self.node_map[start_node][1] + self.node_map[end_node][1]) // 2,
                                        text=letter, fill="#00FF00")
        elif first == 3:
            if direction == None:
                self.canvas.create_line(self.node_map[start_node][0],
                                        self.node_map[start_node][1],
                                        self.node_map[end_node][0],
                                        self.node_map[end_node][1],
                                        fill="#0000FF", width=5)
            elif direction == 0:
                self.canvas.create_line(self.node_map[start_node][0] + self.circle_radius,
                                        self.node_map[start_node][1],
                                        self.node_map[end_node][0],
                                        self.node_map[end_node][1],
                                        fill="#0000FF", width=5)
            elif direction == 1:
                self.canvas.create_line(self.node_map[start_node][0],
                                        self.node_map[start_node][1] + self.circle_radius,
                                        self.node_map[end_node][0],
                                        self.node_map[end_node][1],
                                        fill="#0000FF", width=5)
            elif direction == 2:
                self.canvas.create_line(self.node_map[start_node][0] - self.circle_radius,
                                        self.node_map[start_node][1],
                                        self.node_map[end_node][0],
                                        self.node_map[end_node][1],
                                        fill="#0000FF", width=5)
            elif direction == 3:
                self.canvas.create_line(self.node_map[start_node][0],
                                        self.node_map[start_node][1] - self.circle_radius,
                                        self.node_map[end_node][0],
                                        self.node_map[end_node][1],
                                        fill="#0000FF", width=5)
            if orientation == 'x':
                self.canvas.create_text((self.node_map[start_node][0] + self.node_map[end_node][0]) // 2 + 15,
                                        self.node_map[start_node][1] + text_offset,
                                        text=letter, fill="#0000FF")
            else:
                self.canvas.create_text(self.node_map[start_node][0] + text_offset,
                                        (self.node_map[start_node][1] + self.node_map[end_node][1]) // 2 + 15,
                                        text=letter, fill="#0000FF")

    def draw_shape(self, shape):
        count = 0
        first = shape.first

        for i in shape.edge_list:
            if count == 0:
                self.connect_nodes(i, first, i[2])
            else:
                self.connect_nodes(i, first)
            count += 1

g = Graph(10, 10, 4)
s = Shape(g, [1,-1,1], 0, 0, 1)
g.draw_shape(s)
g.draw()

g = Graph(10, 10, 4)
s = Shape(g, [-1,1,-1], 90, 0, 2)
g.draw_shape(s)
g.draw()

g = Graph(10, 10, 4)
g.draw_layers([1, -1, 1], 1, 2, 0, [-1, 1, -1], 1, 2, 0, 0, 1)
for combination in itertools.product(range(-1,2), repeat=3):
    str1=list(map(int, combination))
    for combination2 in itertools.product(range(-1,2), repeat=3):
        str2=list(map(int, combination2))
        for i in range(1,3):
            for j in range(1,3):
                for k in range(1,3):
                    for l in range(1,3):
                        for m in range(2):
                            for n in range(2):
                                for o in range(4):
                                    g.draw_layers(str1, i, j, 0, str2, k, l, o, m, n)

# for i in range(1,3):
#     for j in range(1,3):
#         for k in range(1,3):
#             for l in range(1,3):
#                 for m in range(2):
#                     for n in range(2):
#                         for o in range(4):
#                             for d in range(1,3):
#                                 for p in range(1,3):
#                                     for q in range(2):
#                                         for r in range(2):
#                                             for s in range(4):
#                                                 for combination in itertools.product(range(-1,2), repeat=3):
#                                                     str1=list(map(int, combination))
#                                                     for combination2 in itertools.product(range(-1,2), repeat=3):
#                                                         str2=list(map(int, combination2))
#                                                         for combination3 in itertools.product(range(-1,2), repeat=3):
#                                                             str3=list(map(int, combination2))
#                                                             g.draw_layers(str1, i, j, 0, str2, k, l, o, m, n,str3,d,p,q,s,r)
a = pickle.load(open("matrix_list.p", "rb"))

# g.draw_shape(s)
#
#
# g.draw_shape(y,1,False)
# g.draw_shape(x)
