from tkinter import *


class GraphDraw(object):
    def __init__(self, graph, size, sizex):
        
        self.graph = graph
        self.size = size
        self.sizex = sizex

        self.canvas_width = self.size
        self.canvas_height = self.size

        self.node_map = self.generate_node_map()

    def draw(self):
        master = Tk()
        self.canvas = Canvas(master, width=self.canvas_width, height=self.canvas_height)
        self.canvas.pack()

        self.draw_node_map()

        self.connect_nodes(1, 11)

        mainloop()
    def create_circle(self, x, y, r, **kwargs):
        return self.canvas.create_oval(x-r, y-r, x+r, y+r, **kwargs)
    
    def generate_node_map(self):
        distance=self.size//(self.sizex+1)
        node_map=[]
        for j in range(self.sizex):
            for i in range(self.sizex):
                node_map.append([distance+i*distance,distance+j*distance])
        return (node_map)
    def draw_node_map(self):
        for i in self.node_map:
            self.create_circle(i[0],i[1],10,fill="blue")            
    def connect_nodes(self,start_node,end_node):
        self.canvas.create_line(self.node_map[start_node][0],
                                self.node_map[start_node][1],
                                self.node_map[end_node][0],
                                self.node_map[end_node][1],
                                fill="#476042")






