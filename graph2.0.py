import itertools
import math
import pickle
class Graph(object):
    def __init__(self, graph_sizex, graph_sizey, *args, **kwargs):
        self.graph_sizex=graph_sizex
        self.graph_sizey=graph_sizey
        self.node_list=[]
        self.graph_dict = self.make_square_graph(graph_sizex,graph_sizey)
        self.edge_list=[]
        self.shape_list=[] 
        self.__generate_edges()
        self.matrix=self.generate_matrix(4,4)
        self.alphabet=self.generate_alphabet(4,4)
        self.stable=True
        self.standard_list=[]
        self.matrix_list=[]
        self.debug_list=[]
        #naredi kvadraten graf velikosti sizex*sizey
    def make_square_graph(self,sizex,sizey):
        g={i:[] for i in range(sizex*sizey)}
        for i in g:
            
            if not((int(i)%sizex)-1<0): 
                g[i].append(int(i)-1)
            if not((int(i)-sizex<0)): 
                g[i].append(int(i)-sizex)
            if not(int(i)+1>=sizex*(i//sizex+1)):
                g[i].append(int(i)+1)
            if not(int(i)+sizex>=sizey*sizex): 
                g[i].append(int(i)+sizex)
        return(g)
    def generate_matrix(self,length_f,length_s):
        matrix=[[0 for i in range(length_s*2)] for j in range(length_f)]
        return(matrix)
    def generate_alphabet(self,length_f,length_s):
        alphabet='abcdefghijklmnopqrstuvwxyz'
        f_alphabet=alphabet[:length_f]
        s_alphabet=alphabet[length_f:length_s+length_f]+alphabet[length_f:length_s+length_f].upper()
        return([f_alphabet,s_alphabet])
    
    def make_connection(self,point1,point2,letter=None,orientation=None):
        #poveže dve točki, pri tem ima povezava obliko:[začetna točka,končna točka,št.povezav na tej črti,vzorec ki je nazadnje zapolnil črto] 
        for i in self.edge_list:
            if((i[0]==point1 and i[1]==point2)or(i[0]==point2 and i[1]==point1)):
                i[2]+=1
                i[3]=len(self.shape_list)
                
                if(letter):
                    if(i[4]==0):
                        i[4]=letter
                        i[5]=orientation
                    
                    elif(i[4]=="checked"):
                        self.raise_not_stable()
                        return
                        
                        
                    else:
                        if(self.alphabet[0].find(letter)==-1):
                            alphabet=0
                            if(self.alphabet[0].find(i[4])==-1):
                                self.raise_not_stable()
                                return
                        else:
                            alphabet=1
                            if(self.alphabet[0].find(i[4])!=-1):
                                self.raise_not_stable()
                                return
                        if(alphabet==0):
                            if(self.matrix[self.alphabet[0].index(i[4])][self.alphabet[1].index(letter)]):
                                pass
                            else:
                                if(i[5]!=orientation):
                                    self.matrix[self.alphabet[0].index(i[4])][self.alphabet[1].index(letter)+len(self.alphabet[0])]=1
                                else:
                                    self.matrix[self.alphabet[0].index(i[4])][self.alphabet[1].index(letter)]=1
                        elif(alphabet==1):
                            if(self.matrix[self.alphabet[0].index(letter)][self.alphabet[1].index(i[4])]):
                                pass
                            else:
                                if(i[5]!=orientation):
                                    self.matrix[self.alphabet[0].index(letter)][self.alphabet[1].index(i[4])+len(self.alphabet[0])]=1
                                else:
                                    self.matrix[self.alphabet[0].index(letter)][self.alphabet[1].index(i[4])]=1
                        i[4]="checked"
                            
    def raise_not_stable(self):
        self.edge_list=[]
        self.shape_list=[]
        self.node_list=[]
        self.__generate_edges()
        self.matrix=self.generate_matrix(4,4)
        
        self.stable=False
        
    def shape_generator(self,shape,movex,movey,orientation,startx=0,starty=0):
        for i in range(self.graph_sizey):
            for j in range(math.ceil((self.graph_sizex-startx)/movex)):
    
                if(j<0):
                    foo=Shape(shape,self.graph_sizex*(i*movey)+starty*self.graph_sizex+(j*movex)+startx,self.graph_sizex,orientation)
                else:
                    foo=Shape(shape,self.graph_sizex*(i*movey)+starty*self.graph_sizex+(j*movex)+startx,self.graph_sizex,orientation)
                    
                yield(foo)
    def draw_shape(self,shape,alphabet=0):
        #vzorec nariše v graf, če vzorec preseka levo ali desno mejo, nariše le del ki je mogoč
        _start_point=shape.start_point
        _orientation=shape.orientation
        count=0
        
        #preveri št. koncev v oglišču 
        if(_start_point<len(self.node_list)):
            self.node_list[_start_point][1]+=1
            if(self.node_list[_start_point][1]>2):
                self.raise_not_stable()
                return
        if(_orientation==0):
            if(_start_point%self.graph_sizex==self.graph_sizex-1):
                return
            else:
                self.make_connection(_start_point,_start_point+1,self.alphabet[alphabet][count],_orientation)
                _start_point=_start_point+1
        elif(_orientation==1):
            self.make_connection(_start_point,_start_point+self.graph_sizex,self.alphabet[alphabet][count],_orientation)
            _start_point=_start_point+self.graph_sizex
        elif(_orientation==2):
            if(_start_point%self.graph_sizex==0):
                return
            else:
                self.make_connection(_start_point,_start_point-1,self.alphabet[alphabet][count],_orientation)
                _start_point=_start_point-1
        elif(_orientation==3):
            self.make_connection(_start_point,_start_point-self.graph_sizex,self.alphabet[alphabet][count],_orientation)
            _start_point=_start_point-self.graph_sizex
        count+=1
        for i in shape.shape:
            if(i==-1): 
                _orientation-=1
                if(_orientation<0):
                    _orientation+=4
            elif(i==1):
                _orientation+=1
                if(_orientation>=4):
                    _orientation-=4
            if(_orientation==0):
                if(_start_point%self.graph_sizex==self.graph_sizex-1):
                    return
                else:
                    self.make_connection(_start_point,_start_point+1,self.alphabet[alphabet][count],_orientation)
                    _start_point=_start_point+1
            elif(_orientation==1):
                self.make_connection(_start_point,_start_point+self.graph_sizex,self.alphabet[alphabet][count],_orientation)
                _start_point=_start_point+self.graph_sizex
            elif(_orientation==2):
                if(_start_point%self.graph_sizex==0):
                    return
                else:
                    self.make_connection(_start_point,_start_point-1,self.alphabet[alphabet][count],_orientation)
                _start_point=_start_point-1
            elif(_orientation==3):
                self.make_connection(_start_point,_start_point-self.graph_sizex,self.alphabet[alphabet][count],_orientation)
                _start_point=_start_point-self.graph_sizex
            count+=1
            
        #preveri št. koncev v oglišču 
        if(_start_point<len(self.node_list) and _start_point>=0):
            self.node_list[_start_point][1]+=1
            if(self.node_list[_start_point][1]>2):
                self.raise_not_stable()
                return
    
    def draw_first_layer(self,shape,movex,movey,num_of_x,num_of_up='neki'):
        #nariše paralelogram vzorcev glede na premike
        count=0
        if(num_of_up=='neki'):
            num_of_up=num_of_x
        for i in range(num_of_up):
            for j in range(num_of_x):
                foo=Shape(shape,self.graph_sizex*(i*movey)+(j*movex)+count,self.graph_sizex,0)
                self.shape_list.append(foo)
                self.draw_shape(foo)
            count+=movex
    
    def draw_layer(self,shape,movex,movey,orientation,startx=0,starty=0):
        #zapolni mrežo z slojem istih vzorcev iste oblike
        for foo in self.shape_generator(shape,movex,movey,orientation,startx,starty):
            self.shape_list.append(foo)
            self.draw_shape(foo)
    def draw_layers(self,shape1,movex1,movey1,orientation1,shape2,movex2,movey2,orientation2,startx=0,starty=0):
       
            first_generator=self.shape_generator(shape1,movex1,movey1,orientation1)
            second_generator=self.shape_generator(shape2,movex2,movey2,orientation2,startx,starty)
            for i in first_generator:
                if(self.stable==False):
                    self.stable=True
                    return
                try:
                    foo=next(second_generator)
                    self.shape_list.append(foo)
                    self.draw_shape(foo,1)
                except:
                    pass
                self.shape_list.append(i)
                self.draw_shape(i)

                
            if(self.check(16,48)):
                flag=True
                for i in range(len(self.matrix_list)):
                    
                    if(self.matrix_list[i][0]==self.matrix):
                        self.matrix_list[i].append([shape1,shape2])
                        flag=False
                        break
                    
                    
                if(flag==True):
                    self.matrix_list.append([self.matrix])
                    self.matrix_list[-1].append([shape1,shape2])
                
                
                file.write(str(shape1)+' '+str(shape2)+' '+str(self.matrix)+' '+str(movex1)+' '+str(movey1)+' '+str(movex2)+' '+str(movey2)+' '+str(orientation2)+' '+str(startx)+' '+str(starty)+'\n')
                self.debug_list.append([shape1,shape2,self.matrix,movex1,movey1,movex2,movey2,orientation2,startx,starty])
            self.raise_not_stable()
            self.stable=True
                
                    
        
    def check(self,first_point,second_point):
        #preveri, če so vse povezavi ali dvojne ali 0 v kvadratu v mreži z nasprotnima ogliščema first_point in second_point
        for i in self.edge_list:
            if(i[0]%self.graph_sizex>=first_point%self.graph_sizex and
               i[0]%self.graph_sizex<=second_point%self.graph_sizex and
               i[1]%self.graph_sizex>=first_point%self.graph_sizex and
               i[1]%self.graph_sizex<=second_point%self.graph_sizex and
               i[0]>=first_point and i[1]>=first_point and
               i[0]<=second_point and i[1]<=second_point):
                if(i[2]==2 or i[2]==0):
                    pass
                else:
                    return(False)
        return(True)
    def __generate_edges(self):
        #zgenerira prvotno stanje grafa
        edges = []
        for vertex in self.graph_dict:
            self.node_list.append([vertex,0])
            for neighbour in self.graph_dict[vertex]:
                if [neighbour, vertex,0,0,0,0] not in edges:
                    edges.append([vertex, neighbour, 0, 0, 0, 0])
        self.edge_list=edges
        
        return edges
    def standardize(self,shape): 
        flag=True
        count=0
        while(count<len(shape)):
            if(shape[count]==-1):
                flag=False
            if(shape[count]==1 and flag==True):
                break
            if(flag==False):
                shape[count]=-shape[count]
            count+=1
        return(shape)
    def standardize_pair(self,pair):
        pair[0]=self.standardize(pair[0])
        pair[1]=self.standardize(pair[1])
        return(pair)
    def reverse(self,shape):

        new=[]
        for i in shape:
            new.append(-i)
        return(new)
    def clean(self):
        count=0
        for i in self.matrix_list:
            
            curr=0
            for j in i[1:]:
                print(curr,j)
                if(curr!=0):
                    if((j[0]==curr[0] and j[1]==curr[1])or(j[0]==curr[1]and j[1]==curr[0]) or
                       (self.reverse(j[0])==curr[0] and self.reverse(j[1])==curr[1])or(self.reverse(j[0])==curr[1]and self.reverse(j[1])==curr[0])):
                        print("neki")
                    else:
                        self.matrix_list.remove(i)
                        break


                curr=j
            count+=1    
class Shape(object):
    """
    shape-vzorec po katerem se lomi veriga(npr. [0,1,-1,0], 0-nadaljuj v isti smeri;1-lomi desno;2-lomi levo)
    orientation-začetna orientacija(0-levo,1-dol,2-desno,3-gor)
    start_point-začetna točka
    sizex: dolžina ene vrstice v grafu
    
    """
    def __init__(self, shape, start_point, sizex, orientation):
        self.shape=shape
        self.start_point=start_point
        self.orientation=orientation
        self.sizex=sizex
        self.edge_list=self.init_edges()
        
        
    def init_edges(self):
        #zapolni edge_list z stranicami oblike([začetna točka,končna točka,črka,orientacija])
        _start_point=self.start_point
        _orientation=self.orientation
        temp=[]
        count=0
        
        
        if(_orientation==0):
            temp.append([_start_point,_start_point+1,_orientation])
            _start_point=_start_point+1
        elif(_orientation==1):
            temp.append([_start_point,_start_point+self.sizex,_orientation])
            _start_point=_start_point+self.sizex
        elif(_orientation==2):
            temp.append([_start_point,_start_point-1,_orientation])
            _start_point=_start_point-1
        elif(_orientation==3):
            temp.append([_start_point,_start_point-self.sizex,_orientation])
            _start_point=_start_point-self.sizex

        for i in self.shape:
            if(i==-1): 
                _orientation-=1
                if(_orientation<0):
                    _orientation+=4
            elif(i==1):
                _orientation+=1
                if(_orientation>=4):
                    _orientation-=4
            if(_orientation==0):
                temp.append([_start_point,_start_point+1,_orientation])
                _start_point=_start_point+1
            elif(_orientation==1):
                temp.append([_start_point,_start_point+self.sizex,_orientation])
                _start_point=_start_point+self.sizex
            elif(_orientation==2):
                temp.append([_start_point,_start_point-1,_orientation])
                _start_point=_start_point-1
            elif(_orientation==3):
                temp.append([_start_point,_start_point-self.sizex,_orientation])
                _start_point=_start_point-self.sizex
            count+=1

        return(temp)
    
    def get_size(self):
        #vrne velikost vzorca
        _start_point=self.start_point
        _orientation=self.orientation
        count=0
        minx=9999
        maxx=0
        miny=9999
        maxy=0
        if(_start_point%self.sizex<minx):
            minx=_start_point%self.sizex
        if(_start_point%self.sizex>maxx):
            maxx=_start_point%self.sizex
        if(_start_point//self.sizex>maxy):
            maxy=_start_point//self.sizex
        if(_start_point//self.sizex<miny):
            miny=_start_point//self.sizex
        if(_orientation==0):
            _start_point=_start_point+1
        elif(_orientation==1):
            _start_point=_start_point+self.sizex
        elif(_orientation==2):
            _start_point=_start_point-1
        elif(_orientation==3):
            _start_point=_start_point-self.sizex
        if(_start_point%self.sizex<minx):
            minx=_start_point%self.sizex
        if(_start_point%self.sizex>maxx):
            maxx=_start_point%self.sizex
        if(_start_point//self.sizex>maxy):
            maxy=_start_point//self.sizex
        if(_start_point//self.sizex<miny):
            miny=_start_point//self.sizex    
        for i in self.shape:
            

            if(i==-1): 
                _orientation-=1
                if(_orientation<0):
                    _orientation+=4
            elif(i==1):
                _orientation+=1
                if(_orientation>=4):
                    _orientation-=4
            if(_orientation==0):
                _start_point=_start_point+1
            elif(_orientation==1):
                _start_point=_start_point+self.sizex
            elif(_orientation==2):
                _start_point=_start_point-1
            elif(_orientation==3):
                _start_point=_start_point-self.sizex
            count+=1
            if(_start_point%self.sizex<minx):
                minx=_start_point%self.sizex
            if(_start_point%self.sizex>maxx):
                maxx=_start_point%self.sizex
            if(_start_point//self.sizex>maxy):
                maxy=_start_point//self.sizex
            if(_start_point//self.sizex<miny):
                miny=_start_point//self.sizex

        return((maxx-minx+1,maxy-miny+1))

g=Graph(7,7)
g.draw_layer([0,1],2,1,0)

##with open("raw.txt", "w") as file:
##    with open("matrix_list","w") as file_matrix:
##        for combination in itertools.product(range(-1,2), repeat=3):
##            str1=list(map(int, combination))
##       
##            for combination2 in itertools.product(range(-1,2), repeat=3):
##                str2=list(map(int, combination2))
##                for i in range(1,3):
##                    for j in range(1,3):
##                        for k in range(1,3):
##                            for l in range(1,3):
##                                for m in range(3):
##                                    for n in range(3):
##                                        for o in range(4):
##                                            g.draw_layers(str1,i,j,0,str2,k,l,o,m,n)
##            
##            pickle.dump(g.matrix_list, open( "matrix_list.p", "wb" ) )
##            pickle.dump(g.debug_list, open( "debug_list.p", "wb" ) )
##            
a=pickle.load(open("debug_list.p","rb"))
                            

def get_graph_size(shape_sizex,shape_sizey,movex,movey,num_of_x,num_of_up='neki'):
    #vrne potrebno velikost grafa, da bo v sebi lahko vseboval paralelogram vzorcev glede na njihovo velikost
    if(num_of_up=='neki'):
        num_of_up=num_of_x
    sizex=shape_sizex+(num_of_x-1)*movex+movex*(num_of_up-1)
    sizey=shape_sizey+(num_of_up-1)*movey
    return((sizex,sizey))

from tkinter import *


class Graph_draw(object):
    def __init__(self,graph,size,sizex,foo):
        
        self.graph=graph
        self.size=size
        self.sizex=sizex

        self.canvas_width = self.size
        self.canvas_height = self.size

        self.node_map=self.generate_node_map()

        self.foo=foo
    def draw(self):
        master = Tk()
        self.canvas = Canvas(master, width=self.canvas_width, height=self.canvas_height)
        self.canvas.pack()

        self.draw_node_map()
        for i in self.foo:
            for j in i.edge_list:
                try:
                    self.connect_nodes(j[0],j[1])
                except:
                    pass
        mainloop()
    def create_circle(self, x, y, r, **kwargs):
        return self.canvas.create_oval(x-r, y-r, x+r, y+r, **kwargs)
    
    def generate_node_map(self):
        distance=self.size//(self.sizex+1)
        node_map=[]
        for j in range(self.sizex):
            for i in range(self.sizex):
                node_map.append([distance+i*distance,distance+j*distance])
        return(node_map)
    def draw_node_map(self):
        for i in self.node_map:
            self.create_circle(i[0],i[1],10,fill="blue")            
    def connect_nodes(self,start_node,end_node):
        self.canvas.create_line(self.node_map[start_node][0],
                                self.node_map[start_node][1],
                                self.node_map[end_node][0],
                                self.node_map[end_node][1],
                                fill="#476042")
            
    

