import itertools
class Graph(object):
    def __init__(self, graph_dict={}, *args, **kwargs):
        self.graph_dict = graph_dict
        self.edge_list=[]
        self.shape_list=[]
        self.__generate_edges()
        self.size=self
    #poveže dve točki, pri tem ima povezava obliko:[začetna točka,končna točka,št.povezav na tej črti,vzorec ki je nazadnje zapolnil črto,robna črta(za končno pregledovanje mreže)] 
    def make_connection(self,point1,point2):
        for i in self.edge_list:
            if((i[0]==point1 and i[1]==point2)or(i[0]==point2 and i[1]==point1)):
                i[2]+=1
                i[3]=len(self.shape_list)
    #nariše prvi vzorec in nato na vsako stranico vzorca nov vzorec tako da je vse zapolnjeno
    def fill_shapes(self,shape, combination, start_point,  orientation):
        self.shape_list.append(Shape(shape,combination, start_point,  orientation))
        self.draw_shape(self.shape_list[0])
        for i in self.shape_list:
            for j in i.edge_list:
                #preveri vzorec, nato zrcalni vzorec, če ustreza pogojem ga nariše
                #NAPAKA:Če je pri prvem true in drugem false kot bi moralo biti ne deluje, če je oboje false pa čudežno dela,bom popravil ko najdem napako
                b=self.draw_shape_onedge(shape,combination,j,False)
                if(b.check(self)):
                    self.shape_list.append(b)
                    self.draw_shape(b)
                c=self.draw_shape_onedge(shape,combination,j,False)
                if(c.check(self)):
                    self.shape_list.append(c)
                    self.draw_shape(c)
    #za dano stranico vrne vzorec, ki se prilega tej stranici
    #mirror_shape- če je true vrne vzorec zrcaljen čez stranico
    def draw_shape_onedge(self,shape,combination,edge,mirror_shape=False): 
        count=1
        count_var=0
        start_point=edge[0]
        reverse_shape=True
        
        direction=edge[3]
        position=0
        character=''
        if(mirror_shape==False):
            for i in range(len(shape)):
                shape[i]=-shape[i]
        for i in combination[1:]:
            for j in i:
                if(j[0]==edge[2]):
                    count_var=count
                    for k in i:
                        if(j != k):
                            character=k[0]
                            
                    if(character==''):
                        character=j[0]
            
            count+=1
       
        for i in combination[count_var]:
            if(len(i)>1):
                if(i[1]=='1'):
                    reverse_shape=False
        for i in combination[0]:
            if(i==character):
                break
            position+=1

        if(reverse_shape==True):
            for i in range(len(shape)):
                shape[i]=-shape[i]
        else:
            if(direction==0):
                start_point=start_point+1
            elif(direction==1):
                start_point=start_point+10     
            elif(direction==2):
                start_point=start_point-1
            elif(direction==3):
                start_point=start_point-10
            direction=direction-2
            if(direction<0):
                direction+=4

        for i in range(position,0,-1):
            direction-=shape[i]
            if(direction<0):
                direction+=4
            elif(direction>3):
                direction-=4
            if(direction==0):
                start_point=start_point-1
            if(direction==1):
                start_point=start_point-10     
            if(direction==2):
                start_point=start_point+1
            if(direction==3):
                start_point=start_point+10
        return(Shape(shape,combination,start_point,direction))
    #vzorec nariše v graf
    def draw_shape(self,shape):
        _start_point=shape.start_point
        _orientation=shape.orientation
        count=0
        for i in shape.shape:
            if(i==-1): 
                _orientation-=1
                if(_orientation<0):
                    _orientation+=4
            elif(i==1):
                _orientation+=1
                if(_orientation>4):
                    _orientation-=4
            if(_orientation==0):
                self.make_connection(_start_point,_start_point+1)
                _start_point=_start_point+1
            elif(_orientation==1):
                self.make_connection(_start_point,_start_point+10)
                _start_point=_start_point+10
            elif(_orientation==2):
                self.make_connection(_start_point,_start_point-1)
                _start_point=_start_point-1
            elif(_orientation==3):
                self.make_connection(_start_point,_start_point-10)
                _start_point=_start_point-10
            count+=1            
    #zgenerira prvotno stanje grafa
    def __generate_edges(self):
        edges = []
        for vertex in self.graph_dict:
            for neighbour in self.graph_dict[vertex]:
                if [neighbour, vertex,0,0,0] not in edges:
                    edges.append([vertex, neighbour, 0, 0, 0])
        self.edge_list=edges
        for i in edges:
            for j in i:
                if(i[0]%10==0 or i[0]%10==9 or (i[0]<10 and i[1]<10) or i[0]>=90):
                    i[4]=1
        return edges


class Shape(object):
    """
    shape-vzorec po katerem se lomi veriga(npr. [0,1,-1,0], 0-nadaljuj v isti smeri;1-lomi desno;2-lomi levo)
    combination-veriga y vezavnimi značilnosti(npr. [['a','b','c','d'],['a','c1'],['b','d']])
               -prvi element je abeceda, v naslednjih seznamih je kateri odseki se vežejo, če je za črko 1 pomeni antiparalelno
    orientation-začetna orientacija(0-levo,1-dol,2-desno,3-gor)
    start_point-začetna točka
    
    """
    def __init__(self, shape, combination, start_point,  orientation, *args, **kwargs):
        self.shape=shape
        self.start_point=start_point
        self.orientation=orientation
        self.combination=combination
        self.edge_list=self.init_edges()
    
    def init_edges(self):
        #zapolni edge_list z stranicami oblike([začetna točka,končna točka,črka,orientacija])
        _start_point=self.start_point
        _orientation=self.orientation
        temp=[]
        count=0
        for i in self.shape:
            if(i==-1): 
                _orientation-=1
                if(_orientation<0):
                    _orientation+=4
            elif(i==1):
                _orientation+=1
                if(_orientation>=4):
                    _orientation-=4
            if(_orientation==0):
                temp.append([_start_point,_start_point+1,self.combination[0][count],_orientation])
                _start_point=_start_point+1
            elif(_orientation==1):
                temp.append([_start_point,_start_point+10,self.combination[0][count],_orientation])
                _start_point=_start_point+10
            elif(_orientation==2):
                temp.append([_start_point,_start_point-1,self.combination[0][count],_orientation])
                _start_point=_start_point-1
            elif(_orientation==3):
                temp.append([_start_point,_start_point-10,self.combination[0][count],_orientation])
                _start_point=_start_point-10
            count+=1

        return(temp)
        
    def edge_pair(self,edge):
        #za stranico vrne stranico, ki se po predpisu veže na njo
        count=1
        count_var=0
        start_point=edge[0]
        end_point=edge[1]
        reverse_shape=True
        direction=edge[3]
        position=0
        character=''
        for i in self.combination[1:]:
            for j in i:
                if(j[0]==edge[2]):
                    
                    count_var=count
                    for k in i:
                        if(j != k):
                            character=k[0]
                            
                    if(character==''):
                        character=i[0][0]
                    
            count+=1
       
        for i in self.combination[count_var]:
            if(len(i)>1):
                if(i[1]=='1'):
                    reverse_shape=False
        for i in self.combination[0]:
            if(i==character):
                break
            position+=1

        if(reverse_shape==False):
            start_point,end_point=end_point,start_point
            direction=direction-2
            if(direction<0):
                direction+=4
        
        return([start_point,end_point,character,direction])
    def check(self,in_graph):
        #preveri, če je v in_graph možno narisati ta vzorec na tem mestu, vrne True/False
        dictio=in_graph.graph_dict
        for i in self.edge_list:
            
            _edge_pair=self.edge_pair(i)
            for j in in_graph.edge_list:
                start_point=i[0]
                end_point=i[1]
                
                if(start_point>end_point):
                    start_point,end_point=end_point,start_point
                if(start_point not in dictio.keys()):  
                    return(False)
                if(end_point not in dictio.keys()):
                    return(False)
                if(end_point not in dictio[start_point]):
    
                   
                    return(False)
                if(j[0]==start_point and j[1]==end_point): 
                    if(j[2]==2):
                        return(False)    
                    else:
                        pass
                if(j[0]==start_point and j[1]==end_point):
                    if(j[3])==0:
                        pass
                    else:
                        
                        if(_edge_pair not in in_graph.shape_list[j[3]-1].edge_list):
                            return(False)
                   
        return(True)
if __name__ == "__main__":
    #naredi kvadraten graf velikosti node_size*node_size
    def make_square_graph(size):
        g={i:[] for i in range(size**2)}
        for i in g:
            if not((int(i)%size)-1<0): 
                g[i].append(int(i)-1)
            if not((int(i)-size<0)): 
                g[i].append(int(i)-size)
            if not(int(i)+1>=size*(i//10+1)):
                g[i].append(int(i)+1)
            if not(int(i)+size>=size**2): 
                g[i].append(int(i)+size)
        return(g)
    #generiranje oblik, moram razmisliti kako bi bolje opredelil, ne deluje
    def generate_shapes(chain_length):
        shapes=[]
        for i in range(-1,2):
            for j in range(-1,2):
                for k in range(-1,2):
                    shapes.append([0,i,j,k])
        return(shapes)
    #generiranje kombinacij, moram naresti z matrikami, ne deluje
    def generate_combinations(chain_length):
        string = 'abcdefghijklmn'
        list_save=list(string[:chain_length])

        combination_list=[]
        for i in range(2,chain_length):
            for j in itertools.combinations(list_save,i):
                temp=[]
                temp2=[]
                for k in list_save:
                    if(k not in j):
                        temp.append(k)
                    else:
                        temp2.append(k)
                
                
                combination_list.append([temp2,temp])
        return([list_save,combination_list])
    
    graph = Graph(make_square_graph(10))
##    s=Shape([0,1,1],15,[['a','b','c'],['a','c1'],['b']],0)
##    graph.draw_shape(s)
##    sez=[]
##    sez.append(s)
##    for i in s.edge_list:
##        g=graph.draw_shape_onedge(i,[['a','b','c'],['a','c1'],['b']],[0,1,1])
##        sez.append(g)
##        graph.draw_shape(g)
##    print(graph.edge_list)
##    for i in sez[1].edge_list:
##        g=graph.draw_shape_onedge(i,[['a','b','c'],['a','c1'],['b']],[0,1,1])
##        if(g.check(graph)==True):
##            graph.draw_shape(g)        
##    print("Vertices of graph:")
##    print(graph.vertices())
##
##    print("Edges of graph:")
##    print(graph.edges())
##
##  
